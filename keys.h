#include<linux/input.h>

int resolveKeyValue(char *key) {
    
        if(strcmp(key,"KEY_RESERVED")==0) return 0;
        if(strcmp(key,"KEY_ESC")==0) return 	1;
        if(strcmp(key,"KEY_1")==0) return 	2;
        if(strcmp(key,"KEY_2")==0) return 	3;
        if(strcmp(key,"KEY_3")==0) return 	4;
        if(strcmp(key,"KEY_4")==0) return 	5;
        if(strcmp(key,"KEY_5")==0) return 	6;
        if(strcmp(key,"KEY_6")==0) return 	7;
        if(strcmp(key,"KEY_7")==0) return 	8;
        if(strcmp(key,"KEY_8")==0) return 	9;
        if(strcmp(key,"KEY_9")==0) return 	10;
        if(strcmp(key,"KEY_0")==0) return 	11;
        if(strcmp(key,"KEY_MINUS")==0) return 12;
        if(strcmp(key,"KEY_EQUAL")==0) return 13;
        if(strcmp(key,"KEY_BACKSPACE")==0) return 14;
        if(strcmp(key,"KEY_TAB")==0) return 	15;
        if(strcmp(key,"KEY_Q")==0) return 	16;
        if(strcmp(key,"KEY_W")==0) return 	17;
        if(strcmp(key,"KEY_E")==0) return 	18;
        if(strcmp(key,"KEY_R")==0) return 	19;
        if(strcmp(key,"KEY_T")==0) return 	20;
        if(strcmp(key,"KEY_Y")==0) return 	21;
        if(strcmp(key,"KEY_U")==0) return 	22;
        if(strcmp(key,"KEY_I")==0) return 	23;
        if(strcmp(key,"KEY_O")==0) return 	24;
        if(strcmp(key,"KEY_P")==0) return 	25;
        if(strcmp(key,"KEY_LEFTBRACE")==0) return 26;
        if(strcmp(key,"KEY_RIGHTBRACE")==0) return 27;
        if(strcmp(key,"KEY_ENTER")==0) return 28;
        if(strcmp(key,"KEY_LEFTCTRL")==0) return 29;
        if(strcmp(key,"KEY_A")==0) return 	30;
        if(strcmp(key,"KEY_S")==0) return 	31;
        if(strcmp(key,"KEY_D")==0) return 	32;
        if(strcmp(key,"KEY_F")==0) return 	33;
        if(strcmp(key,"KEY_G")==0) return 	34;
        if(strcmp(key,"KEY_H")==0) return 	35;
        if(strcmp(key,"KEY_J")==0) return 	36;
        if(strcmp(key,"KEY_K")==0) return 	37;
        if(strcmp(key,"KEY_L")==0) return 	38;
        if(strcmp(key,"KEY_SEMICOLON")==0) return 39;
        if(strcmp(key,"KEY_APOSTROPHE")==0) return 40;
        if(strcmp(key,"KEY_GRAVE")==0) return 41;
        if(strcmp(key,"KEY_LEFTSHIFT")==0) return 42;
        if(strcmp(key,"KEY_BACKSLASH")==0) return 43;
        if(strcmp(key,"KEY_Z")==0) return 	44;
        if(strcmp(key,"KEY_X")==0) return 	45;
        if(strcmp(key,"KEY_C")==0) return 	46;
        if(strcmp(key,"KEY_V")==0) return 	47;
        if(strcmp(key,"KEY_B")==0) return 	48;
        if(strcmp(key,"KEY_N")==0) return 	49;
        if(strcmp(key,"KEY_M")==0) return 	50;
        if(strcmp(key,"KEY_COMMA")==0) return 51;
        if(strcmp(key,"KEY_DOT")==0) return 	52;
        if(strcmp(key,"KEY_SLASH")==0) return 53;
        if(strcmp(key,"KEY_RIGHTSHIFT")==0) return 54;
        if(strcmp(key,"KEY_KPASTERISK")==0) return 55;
        if(strcmp(key,"KEY_LEFTALT")==0) return 56;
        if(strcmp(key,"KEY_SPACE")==0) return 57;
        if(strcmp(key,"KEY_CAPSLOCK")==0) return 58;
        if(strcmp(key,"KEY_F1")==0) return 	59;
        if(strcmp(key,"KEY_F2")==0) return 	60;
        if(strcmp(key,"KEY_F3")==0) return 	61;
        if(strcmp(key,"KEY_F4")==0) return 	62;
        if(strcmp(key,"KEY_F5")==0) return 	63;
        if(strcmp(key,"KEY_F6")==0) return 	64;
        if(strcmp(key,"KEY_F7")==0) return 	65;
        if(strcmp(key,"KEY_F8")==0) return 	66;
        if(strcmp(key,"KEY_F9")==0) return 	67;
        if(strcmp(key,"KEY_F10")==0) return 	68;
        if(strcmp(key,"KEY_NUMLOCK")==0) return 69;
        if(strcmp(key,"KEY_SCROLLLOCK")==0) return 70;
        if(strcmp(key,"KEY_KP7")==0) return 	71;
        if(strcmp(key,"KEY_KP8")==0) return 	72;
        if(strcmp(key,"KEY_KP9")==0) return 	73;
        if(strcmp(key,"KEY_KPMINUS")==0) return 74;
        if(strcmp(key,"KEY_KP4")==0) return 	75;
        if(strcmp(key,"KEY_KP5")==0) return 	76;
        if(strcmp(key,"KEY_KP6")==0) return 	77;
        if(strcmp(key,"KEY_KPPLUS")==0) return 78;
        if(strcmp(key,"KEY_KP1")==0) return 	79;
        if(strcmp(key,"KEY_KP2")==0) return 	80;
        if(strcmp(key,"KEY_KP3")==0) return 	81;
        if(strcmp(key,"KEY_KP0")==0) return 	82;
        if(strcmp(key,"KEY_KPDOT")==0) return 83;

        if(strcmp(key,"KEY_ZENKAKUHANKAKU")==0)	85;
        if(strcmp(key,"KEY_102ND")==0) return 86;
        if(strcmp(key,"KEY_F11")==0) return 	87;
        if(strcmp(key,"KEY_F12")==0) return 	88;
        if(strcmp(key,"KEY_RO")==0) return 	89;
        if(strcmp(key,"KEY_KATAKANA")==0) return 90;
        if(strcmp(key,"KEY_HIRAGANA")==0) return 91;
        if(strcmp(key,"KEY_HENKAN")==0) return 92;
        if(strcmp(key,"KEY_KATAKANAHIRAGANA")==0)	93;
        if(strcmp(key,"KEY_MUHENKAN")==0) return 94;
        if(strcmp(key,"KEY_KPJPCOMMA")==0) return 95;
        if(strcmp(key,"KEY_KPENTER")==0) return 96;
        if(strcmp(key,"KEY_RIGHTCTRL")==0) return 97;
        if(strcmp(key,"KEY_KPSLASH")==0) return 98;
        if(strcmp(key,"KEY_SYSRQ")==0) return 99;
        if(strcmp(key,"KEY_RIGHTALT")==0) return 100;
        if(strcmp(key,"KEY_LINEFEED")==0) return 101;
        if(strcmp(key,"KEY_HOME")==0) return 102;
        if(strcmp(key,"KEY_UP")==0) return 	103;
        if(strcmp(key,"KEY_PAGEUP")==0) return 104;
        if(strcmp(key,"KEY_LEFT")==0) return 105;
        if(strcmp(key,"KEY_RIGHT")==0) return 106;
        if(strcmp(key,"KEY_END")==0) return 	107;
        if(strcmp(key,"KEY_DOWN")==0) return 108;
        if(strcmp(key,"KEY_PAGEDOWN")==0) return 109;
        if(strcmp(key,"KEY_INSERT")==0) return 110;
        if(strcmp(key,"KEY_DELETE")==0) return 111;
        if(strcmp(key,"KEY_MACRO")==0) return 112;
        if(strcmp(key,"KEY_MUTE")==0) return 113;
        if(strcmp(key,"KEY_VOLUMEDOWN")==0) return 114;
        if(strcmp(key,"KEY_VOLUMEUP")==0) return 115;
        if(strcmp(key,"KEY_POWER")==0) return 116	/* SC System Power Down */;
        if(strcmp(key,"KEY_KPEQUAL")==0) return 117;
        if(strcmp(key,"KEY_KPPLUSMINUS")==0) return 118;
        if(strcmp(key,"KEY_PAUSE")==0) return 119;
        if(strcmp(key,"KEY_SCALE")==0) return 120	/* AL Compiz Scale (Expose) */;

        if(strcmp(key,"KEY_KPCOMMA")==0) return 121;
        if(strcmp(key,"KEY_HANGEUL")==0) return 122;
        if(strcmp(key,"KEY_HANGUEL")==0) return KEY_HANGEUL;
        if(strcmp(key,"KEY_HANJA")==0) return 123;
        if(strcmp(key,"KEY_YEN")==0) return 	124;
        if(strcmp(key,"KEY_LEFTMETA")==0) return 125;
        if(strcmp(key,"KEY_RIGHTMETA")==0) return 126;
        if(strcmp(key,"KEY_COMPOSE")==0) return 127;

        if(strcmp(key,"KEY_STOP")==0) return 128	/* AC Stop */;
        if(strcmp(key,"KEY_AGAIN")==0) return 129;
        if(strcmp(key,"KEY_PROPS")==0) return 130	/* AC Properties */;
        if(strcmp(key,"KEY_UNDO")==0) return 131	/* AC Undo */;
        if(strcmp(key,"KEY_FRONT")==0) return 132;
        if(strcmp(key,"KEY_COPY")==0) return 133	/* AC Copy */;
        if(strcmp(key,"KEY_OPEN")==0) return 134	/* AC Open */;
        if(strcmp(key,"KEY_PASTE")==0) return 135	/* AC Paste */;
        if(strcmp(key,"KEY_FIND")==0) return 136	/* AC Search */;
        if(strcmp(key,"KEY_CUT")==0) return 	137	/* AC Cut */;
        if(strcmp(key,"KEY_HELP")==0) return 138	/* AL Integrated Help Center */;
        if(strcmp(key,"KEY_MENU")==0) return 139	/* Menu (show menu) */;
        if(strcmp(key,"KEY_CALC")==0) return 140	/* AL Calculator */;
        if(strcmp(key,"KEY_SETUP")==0) return 141;
        if(strcmp(key,"KEY_SLEEP")==0) return 142	/* SC System Sleep */;
        if(strcmp(key,"KEY_WAKEUP")==0) return 143	/* System Wake Up */;
        if(strcmp(key,"KEY_FILE")==0) return 144	/* AL Local Machine Browser */;
        if(strcmp(key,"KEY_SENDFILE")==0) return 145;
        if(strcmp(key,"KEY_DELETEFILE")==0) return 146;
        if(strcmp(key,"KEY_XFER")==0) return 147;
        if(strcmp(key,"KEY_PROG1")==0) return 148;
        if(strcmp(key,"KEY_PROG2")==0) return 149;
        if(strcmp(key,"KEY_WWW")==0) return 	150	/* AL Internet Browser */;
        if(strcmp(key,"KEY_MSDOS")==0) return 151;
        if(strcmp(key,"KEY_COFFEE")==0) return 152	/* AL Terminal Lock/Screensaver */;
        if(strcmp(key,"KEY_SCREENLOCK")==0) return KEY_COFFEE;
        if(strcmp(key,"KEY_DIRECTION")==0) return 153;
        if(strcmp(key,"KEY_CYCLEWINDOWS")==0)	154;
        if(strcmp(key,"KEY_MAIL")==0) return 155;
        if(strcmp(key,"KEY_BOOKMARKS")==0) return 156	/* AC Bookmarks */;
        if(strcmp(key,"KEY_COMPUTER")==0) return 157;
        if(strcmp(key,"KEY_BACK")==0) return 158	/* AC Back */;
        if(strcmp(key,"KEY_FORWARD")==0) return 159	/* AC Forward */;
        if(strcmp(key,"KEY_CLOSECD")==0) return 160;
        if(strcmp(key,"KEY_EJECTCD")==0) return 161;
        if(strcmp(key,"KEY_EJECTCLOSECD")==0)	162;
        if(strcmp(key,"KEY_NEXTSONG")==0) return 163;
        if(strcmp(key,"KEY_PLAYPAUSE")==0) return 164;
        if(strcmp(key,"KEY_PREVIOUSSONG")==0)	165;
        if(strcmp(key,"KEY_STOPCD")==0) return 166;
        if(strcmp(key,"KEY_RECORD")==0) return 167;
        if(strcmp(key,"KEY_REWIND")==0) return 168;
        if(strcmp(key,"KEY_PHONE")==0) return 169	/* Media Select Telephone */;
        if(strcmp(key,"KEY_ISO")==0) return 	170;
        if(strcmp(key,"KEY_CONFIG")==0) return 171	/* AL Consumer Control Configuration */;
        if(strcmp(key,"KEY_HOMEPAGE")==0) return 172	/* AC Home */;
        if(strcmp(key,"KEY_REFRESH")==0) return 173	/* AC Refresh */;
        if(strcmp(key,"KEY_EXIT")==0) return 174	/* AC Exit */;
        if(strcmp(key,"KEY_MOVE")==0) return 175;
        if(strcmp(key,"KEY_EDIT")==0) return 176;
        if(strcmp(key,"KEY_SCROLLUP")==0) return 177;
        if(strcmp(key,"KEY_SCROLLDOWN")==0) return 178;
        if(strcmp(key,"KEY_KPLEFTPAREN")==0) return 179;
        if(strcmp(key,"KEY_KPRIGHTPAREN")==0)	180;
        if(strcmp(key,"KEY_NEW")==0) return 	181	/* AC New */;
        if(strcmp(key,"KEY_REDO")==0) return 182	/* AC Redo/Repeat */;

        if(strcmp(key,"KEY_F13")==0) return 	183;
        if(strcmp(key,"KEY_F14")==0) return 	184;
        if(strcmp(key,"KEY_F15")==0) return 	185;
        if(strcmp(key,"KEY_F16")==0) return 	186;
        if(strcmp(key,"KEY_F17")==0) return 	187;
        if(strcmp(key,"KEY_F18")==0) return 	188;
        if(strcmp(key,"KEY_F19")==0) return 	189;
        if(strcmp(key,"KEY_F20")==0) return 	190;
        if(strcmp(key,"KEY_F21")==0) return 	191;
        if(strcmp(key,"KEY_F22")==0) return 	192;
        if(strcmp(key,"KEY_F23")==0) return 	193;
        if(strcmp(key,"KEY_F24")==0) return 	194;

        if(strcmp(key,"KEY_PLAYCD")==0) return 200;
        if(strcmp(key,"KEY_PAUSECD")==0) return 201;
        if(strcmp(key,"KEY_PROG3")==0) return 202;
        if(strcmp(key,"KEY_PROG4")==0) return 203;
        if(strcmp(key,"KEY_DASHBOARD")==0) return 204	/* AL Dashboard */;
        if(strcmp(key,"KEY_SUSPEND")==0) return 205;
        if(strcmp(key,"KEY_CLOSE")==0) return 206	/* AC Close */;
        if(strcmp(key,"KEY_PLAY")==0) return 207;
        if(strcmp(key,"KEY_FASTFORWARD")==0) return 208;
        if(strcmp(key,"KEY_BASSBOOST")==0) return 209;
        if(strcmp(key,"KEY_PRINT")==0) return 210	/* AC Print */;
        if(strcmp(key,"KEY_HP")==0) return 	211;
        if(strcmp(key,"KEY_CAMERA")==0) return 212;
        if(strcmp(key,"KEY_SOUND")==0) return 213;
        if(strcmp(key,"KEY_QUESTION")==0) return 214;
        if(strcmp(key,"KEY_EMAIL")==0) return 215;
        if(strcmp(key,"KEY_CHAT")==0) return 216;
        if(strcmp(key,"KEY_SEARCH")==0) return 217;
        if(strcmp(key,"KEY_CONNECT")==0) return 218;
        if(strcmp(key,"KEY_FINANCE")==0) return 219	/* AL Checkbook/Finance */;
        if(strcmp(key,"KEY_SPORT")==0) return 220;
        if(strcmp(key,"KEY_SHOP")==0) return 221;
        if(strcmp(key,"KEY_ALTERASE")==0) return 222;
        if(strcmp(key,"KEY_CANCEL")==0) return 223	/* AC Cancel */;
        if(strcmp(key,"KEY_BRIGHTNESSDOWN")==0)	224;
        if(strcmp(key,"KEY_BRIGHTNESSUP")==0)	225;
        if(strcmp(key,"KEY_MEDIA")==0) return 226;

        if(strcmp(key,"KEY_SWITCHVIDEOMODE")==0)	227	/* Cycle between available video;
        ")==0) return ")==0) return 	   outputs (Monitor/LCD/TV-out/etc) */;
        if(strcmp(key,"KEY_KBDILLUMTOGGLE")==0)	228;
        if(strcmp(key,"KEY_KBDILLUMDOWN")==0)	229;
        if(strcmp(key,"KEY_KBDILLUMUP")==0) return 230;

        if(strcmp(key,"KEY_SEND")==0) return 231	/* AC Send */;
        if(strcmp(key,"KEY_REPLY")==0) return 232	/* AC Reply */;
        if(strcmp(key,"KEY_FORWARDMAIL")==0) return 233	/* AC Forward Msg */;
        if(strcmp(key,"KEY_SAVE")==0) return 234	/* AC Save */;
        if(strcmp(key,"KEY_DOCUMENTS")==0) return 235;

        if(strcmp(key,"KEY_BATTERY")==0) return 236;

        if(strcmp(key,"KEY_BLUETOOTH")==0) return 237;
        if(strcmp(key,"KEY_WLAN")==0) return 238;
        if(strcmp(key,"KEY_UWB")==0) return 	239;

        if(strcmp(key,"KEY_UNKNOWN")==0) return 240;

        if(strcmp(key,"KEY_VIDEO_NEXT")==0) return 241	/* drive next video source */;
        if(strcmp(key,"KEY_VIDEO_PREV")==0) return 242	/* drive previous video source */;
        if(strcmp(key,"KEY_BRIGHTNESS_CYCLE")==0)	243	/* brightness up, after max is min */;
        if(strcmp(key,"KEY_BRIGHTNESS_ZERO")==0)	244	/* brightness off, use ambient */;
        if(strcmp(key,"KEY_DISPLAY_OFF")==0) return 245	/* display device to off state */;

        if(strcmp(key,"KEY_WIMAX")==0) return 246;
        if(strcmp(key,"KEY_RFKILL")==0) return 247	/* Key that controls all radios */;

        if(strcmp(key,"KEY_MICMUTE")==0) return 248	/* Mute / unmute the microphone */;

        /* Code 255 is reserved for special needs of AT keyboard driver */

        if(strcmp(key,"BTN_MISC")==0) return 0x100;
        if(strcmp(key,"BTN_0")==0) return 	0x100;
        if(strcmp(key,"BTN_1")==0) return 	0x101;
        if(strcmp(key,"BTN_2")==0) return 	0x102;
        if(strcmp(key,"BTN_3")==0) return 	0x103;
        if(strcmp(key,"BTN_4")==0) return 	0x104;
        if(strcmp(key,"BTN_5")==0) return 	0x105;
        if(strcmp(key,"BTN_6")==0) return 	0x106;
        if(strcmp(key,"BTN_7")==0) return 	0x107;
        if(strcmp(key,"BTN_8")==0) return 	0x108;
        if(strcmp(key,"BTN_9")==0) return 	0x109;

        if(strcmp(key,"BTN_MOUSE")==0) return 0x110;
        if(strcmp(key,"BTN_LEFT")==0) return 0x110;
        if(strcmp(key,"BTN_RIGHT")==0) return 0x111;
        if(strcmp(key,"BTN_MIDDLE")==0) return 0x112;
        if(strcmp(key,"BTN_SIDE")==0) return 0x113;
        if(strcmp(key,"BTN_EXTRA")==0) return 0x114;
        if(strcmp(key,"BTN_FORWARD")==0) return 0x115;
        if(strcmp(key,"BTN_BACK")==0) return 0x116;
        if(strcmp(key,"BTN_TASK")==0) return 0x117;

        if(strcmp(key,"BTN_JOYSTICK")==0) return 0x120;
        if(strcmp(key,"BTN_TRIGGER")==0) return 0x120;
        if(strcmp(key,"BTN_THUMB")==0) return 0x121;
        if(strcmp(key,"BTN_THUMB2")==0) return 0x122;
        if(strcmp(key,"BTN_TOP")==0) return 	0x123;
        if(strcmp(key,"BTN_TOP2")==0) return 0x124;
        if(strcmp(key,"BTN_PINKIE")==0) return 0x125;
        if(strcmp(key,"BTN_BASE")==0) return 0x126;
        if(strcmp(key,"BTN_BASE2")==0) return 0x127;
        if(strcmp(key,"BTN_BASE3")==0) return 0x128;
        if(strcmp(key,"BTN_BASE4")==0) return 0x129;
        if(strcmp(key,"BTN_BASE5")==0) return 0x12a;
        if(strcmp(key,"BTN_BASE6")==0) return 0x12b;
        if(strcmp(key,"BTN_DEAD")==0) return 0x12f;

        if(strcmp(key,"BTN_GAMEPAD")==0) return 0x130;
        if(strcmp(key,"BTN_A")==0) return 	0x130;
        if(strcmp(key,"BTN_B")==0) return 	0x131;
        if(strcmp(key,"BTN_C")==0) return 	0x132;
        if(strcmp(key,"BTN_X")==0) return 	0x133;
        if(strcmp(key,"BTN_Y")==0) return 	0x134;
        if(strcmp(key,"BTN_Z")==0) return 	0x135;
        if(strcmp(key,"BTN_TL")==0) return 	0x136;
        if(strcmp(key,"BTN_TR")==0) return 	0x137;
        if(strcmp(key,"BTN_TL2")==0) return 	0x138;
        if(strcmp(key,"BTN_TR2")==0) return 	0x139;
        if(strcmp(key,"BTN_SELECT")==0) return 0x13a;
        if(strcmp(key,"BTN_START")==0) return 0x13b;
        if(strcmp(key,"BTN_MODE")==0) return 0x13c;
        if(strcmp(key,"BTN_THUMBL")==0) return 0x13d;
        if(strcmp(key,"BTN_THUMBR")==0) return 0x13e;

        if(strcmp(key,"BTN_DIGI")==0) return 0x140;
        if(strcmp(key,"BTN_TOOL_PEN")==0) return 0x140;
        if(strcmp(key,"BTN_TOOL_RUBBER")==0) return 0x141;
        if(strcmp(key,"BTN_TOOL_BRUSH")==0) return 0x142;
        if(strcmp(key,"BTN_TOOL_PENCIL")==0) return 0x143;
        if(strcmp(key,"BTN_TOOL_AIRBRUSH")==0)	0x144;
        if(strcmp(key,"BTN_TOOL_FINGER")==0) return 0x145;
        if(strcmp(key,"BTN_TOOL_MOUSE")==0) return 0x146;
        if(strcmp(key,"BTN_TOOL_LENS")==0) return 0x147;
        if(strcmp(key,"BTN_TOOL_QUINTTAP")==0)	0x148	/* Five fingers on trackpad */;
        if(strcmp(key,"BTN_TOUCH")==0) return 0x14a;
        if(strcmp(key,"BTN_STYLUS")==0) return 0x14b;
        if(strcmp(key,"BTN_STYLUS2")==0) return 0x14c;
        if(strcmp(key,"BTN_TOOL_DOUBLETAP")==0)	0x14d;
        if(strcmp(key,"BTN_TOOL_TRIPLETAP")==0)	0x14e;
        if(strcmp(key,"BTN_TOOL_QUADTAP")==0)	0x14f	/* Four fingers on trackpad */;

        if(strcmp(key,"BTN_WHEEL")==0) return 0x150;
        if(strcmp(key,"BTN_GEAR_DOWN")==0) return 0x150;
        if(strcmp(key,"BTN_GEAR_UP")==0) return 0x151;

        if(strcmp(key,"KEY_OK")==0) return 	0x160;
        if(strcmp(key,"KEY_SELECT")==0) return 0x161;
        if(strcmp(key,"KEY_GOTO")==0) return 0x162;
        if(strcmp(key,"KEY_CLEAR")==0) return 0x163;
        if(strcmp(key,"KEY_POWER2")==0) return 0x164;
        if(strcmp(key,"KEY_OPTION")==0) return 0x165;
        if(strcmp(key,"KEY_INFO")==0) return 0x166	/* AL OEM Features/Tips/Tutorial */;
        if(strcmp(key,"KEY_TIME")==0) return 0x167;
        if(strcmp(key,"KEY_VENDOR")==0) return 0x168;
        if(strcmp(key,"KEY_ARCHIVE")==0) return 0x169;
        if(strcmp(key,"KEY_PROGRAM")==0) return 0x16a	/* Media Select Program Guide */;
        if(strcmp(key,"KEY_CHANNEL")==0) return 0x16b;
        if(strcmp(key,"KEY_FAVORITES")==0) return 0x16c;
        if(strcmp(key,"KEY_EPG")==0) return 	0x16d;
        if(strcmp(key,"KEY_PVR")==0) return 	0x16e	/* Media Select Home */;
        if(strcmp(key,"KEY_MHP")==0) return 	0x16f;
        if(strcmp(key,"KEY_LANGUAGE")==0) return 0x170;
        if(strcmp(key,"KEY_TITLE")==0) return 0x171;
        if(strcmp(key,"KEY_SUBTITLE")==0) return 0x172;
        if(strcmp(key,"KEY_ANGLE")==0) return 0x173;
        if(strcmp(key,"KEY_ZOOM")==0) return 0x174;
        if(strcmp(key,"KEY_MODE")==0) return 0x175;
        if(strcmp(key,"KEY_KEYBOARD")==0) return 0x176;
        if(strcmp(key,"KEY_SCREEN")==0) return 0x177;
        if(strcmp(key,"KEY_PC")==0) return 	0x178	/* Media Select Computer */;
        if(strcmp(key,"KEY_TV")==0) return 	0x179	/* Media Select TV */;
        if(strcmp(key,"KEY_TV2")==0) return 	0x17a	/* Media Select Cable */;
        if(strcmp(key,"KEY_VCR")==0) return 	0x17b	/* Media Select VCR */;
        if(strcmp(key,"KEY_VCR2")==0) return 0x17c	/* VCR Plus */;
        if(strcmp(key,"KEY_SAT")==0) return 	0x17d	/* Media Select Satellite */;
        if(strcmp(key,"KEY_SAT2")==0) return 0x17e;
        if(strcmp(key,"KEY_CD")==0) return 	0x17f	/* Media Select CD */;
        if(strcmp(key,"KEY_TAPE")==0) return 0x180	/* Media Select Tape */;
        if(strcmp(key,"KEY_RADIO")==0) return 0x181;
        if(strcmp(key,"KEY_TUNER")==0) return 0x182	/* Media Select Tuner */;
        if(strcmp(key,"KEY_PLAYER")==0) return 0x183;
        if(strcmp(key,"KEY_TEXT")==0) return 0x184;
        if(strcmp(key,"KEY_DVD")==0) return 	0x185	/* Media Select DVD */;
        if(strcmp(key,"KEY_AUX")==0) return 	0x186;
        if(strcmp(key,"KEY_MP3")==0) return 	0x187;
        if(strcmp(key,"KEY_AUDIO")==0) return 0x188	/* AL Audio Browser */;
        if(strcmp(key,"KEY_VIDEO")==0) return 0x189	/* AL Movie Browser */;
        if(strcmp(key,"KEY_DIRECTORY")==0) return 0x18a;
        if(strcmp(key,"KEY_LIST")==0) return 0x18b;
        if(strcmp(key,"KEY_MEMO")==0) return 0x18c	/* Media Select Messages */;
        if(strcmp(key,"KEY_CALENDAR")==0) return 0x18d;
        if(strcmp(key,"KEY_RED")==0) return 	0x18e;
        if(strcmp(key,"KEY_GREEN")==0) return 0x18f;
        if(strcmp(key,"KEY_YELLOW")==0) return 0x190;
        if(strcmp(key,"KEY_BLUE")==0) return 0x191;
        if(strcmp(key,"KEY_CHANNELUP")==0) return 0x192	/* Channel Increment */;
        if(strcmp(key,"KEY_CHANNELDOWN")==0) return 0x193	/* Channel Decrement */;
        if(strcmp(key,"KEY_FIRST")==0) return 0x194;
        if(strcmp(key,"KEY_LAST")==0) return 0x195	/* Recall Last */;
        if(strcmp(key,"KEY_AB")==0) return 	0x196;
        if(strcmp(key,"KEY_NEXT")==0) return 0x197;
        if(strcmp(key,"KEY_RESTART")==0) return 0x198;
        if(strcmp(key,"KEY_SLOW")==0) return 0x199;
        if(strcmp(key,"KEY_SHUFFLE")==0) return 0x19a;
        if(strcmp(key,"KEY_BREAK")==0) return 0x19b;
        if(strcmp(key,"KEY_PREVIOUS")==0) return 0x19c;
        if(strcmp(key,"KEY_DIGITS")==0) return 0x19d;
        if(strcmp(key,"KEY_TEEN")==0) return 0x19e;
        if(strcmp(key,"KEY_TWEN")==0) return 0x19f;
        if(strcmp(key,"KEY_VIDEOPHONE")==0) return 0x1a0	/* Media Select Video Phone */;
        if(strcmp(key,"KEY_GAMES")==0) return 0x1a1	/* Media Select Games */;
        if(strcmp(key,"KEY_ZOOMIN")==0) return 0x1a2	/* AC Zoom In */;
        if(strcmp(key,"KEY_ZOOMOUT")==0) return 0x1a3	/* AC Zoom Out */;
        if(strcmp(key,"KEY_ZOOMRESET")==0) return 0x1a4	/* AC Zoom */;
        if(strcmp(key,"KEY_WORDPROCESSOR")==0)	0x1a5	/* AL Word Processor */;
        if(strcmp(key,"KEY_EDITOR")==0) return 0x1a6	/* AL Text Editor */;
        if(strcmp(key,"KEY_SPREADSHEET")==0) return 0x1a7	/* AL Spreadsheet */;
        if(strcmp(key,"KEY_GRAPHICSEDITOR")==0)	0x1a8	/* AL Graphics Editor */;
        if(strcmp(key,"KEY_PRESENTATION")==0)	0x1a9	/* AL Presentation App */;
        if(strcmp(key,"KEY_DATABASE")==0) return 0x1aa	/* AL Database App */;
        if(strcmp(key,"KEY_NEWS")==0) return 0x1ab	/* AL Newsreader */;
        if(strcmp(key,"KEY_VOICEMAIL")==0) return 0x1ac	/* AL Voicemail */;
        if(strcmp(key,"KEY_ADDRESSBOOK")==0) return 0x1ad	/* AL Contacts/Address Book */;
        if(strcmp(key,"KEY_MESSENGER")==0) return 0x1ae	/* AL Instant Messaging */;
        if(strcmp(key,"KEY_DISPLAYTOGGLE")==0)	0x1af	/* Turn display (LCD) on and off */;
        if(strcmp(key,"KEY_SPELLCHECK")==0) return 0x1b0   /* AL Spell Check */;
        if(strcmp(key,"KEY_LOGOFF")==0) return 0x1b1   /* AL Logoff */;

        if(strcmp(key,"KEY_DOLLAR")==0) return 0x1b2;
        if(strcmp(key,"KEY_EURO")==0) return 0x1b3;

        if(strcmp(key,"KEY_FRAMEBACK")==0) return 0x1b4	/* Consumer - transport controls */;
        if(strcmp(key,"KEY_FRAMEFORWARD")==0)	0x1b5;
        if(strcmp(key,"KEY_CONTEXT_MENU")==0)	0x1b6	/* GenDesc - system context menu */;
        if(strcmp(key,"KEY_MEDIA_REPEAT")==0)	0x1b7	/* Consumer - transport control */;
        if(strcmp(key,"KEY_10CHANNELSUP")==0)	0x1b8	/* 10 channels up (10+) */;
        if(strcmp(key,"KEY_10CHANNELSDOWN")==0)	0x1b9	/* 10 channels down (10-) */;
        if(strcmp(key,"KEY_IMAGES")==0) return 0x1ba	/* AL Image Browser */;

        if(strcmp(key,"KEY_DEL_EOL")==0) return 0x1c0;
        if(strcmp(key,"KEY_DEL_EOS")==0) return 0x1c1;
        if(strcmp(key,"KEY_INS_LINE")==0) return 0x1c2;
        if(strcmp(key,"KEY_DEL_LINE")==0) return 0x1c3;

        if(strcmp(key,"KEY_FN")==0) return 	0x1d0;
        if(strcmp(key,"KEY_FN_ESC")==0) return 0x1d1;
        if(strcmp(key,"KEY_FN_F1")==0) return 0x1d2;
        if(strcmp(key,"KEY_FN_F2")==0) return 0x1d3;
        if(strcmp(key,"KEY_FN_F3")==0) return 0x1d4;
        if(strcmp(key,"KEY_FN_F4")==0) return 0x1d5;
        if(strcmp(key,"KEY_FN_F5")==0) return 0x1d6;
        if(strcmp(key,"KEY_FN_F6")==0) return 0x1d7;
        if(strcmp(key,"KEY_FN_F7")==0) return 0x1d8;
        if(strcmp(key,"KEY_FN_F8")==0) return 0x1d9;
        if(strcmp(key,"KEY_FN_F9")==0) return 0x1da;
        if(strcmp(key,"KEY_FN_F10")==0) return 0x1db;
        if(strcmp(key,"KEY_FN_F11")==0) return 0x1dc;
        if(strcmp(key,"KEY_FN_F12")==0) return 0x1dd;
        if(strcmp(key,"KEY_FN_1")==0) return 0x1de;
        if(strcmp(key,"KEY_FN_2")==0) return 0x1df;
        if(strcmp(key,"KEY_FN_D")==0) return 0x1e0;
        if(strcmp(key,"KEY_FN_E")==0) return 0x1e1;
        if(strcmp(key,"KEY_FN_F")==0) return 0x1e2;
        if(strcmp(key,"KEY_FN_S")==0) return 0x1e3;
        if(strcmp(key,"KEY_FN_B")==0) return 0x1e4;

        if(strcmp(key,"KEY_BRL_DOT1")==0) return 0x1f1;
        if(strcmp(key,"KEY_BRL_DOT2")==0) return 0x1f2;
        if(strcmp(key,"KEY_BRL_DOT3")==0) return 0x1f3;
        if(strcmp(key,"KEY_BRL_DOT4")==0) return 0x1f4;
        if(strcmp(key,"KEY_BRL_DOT5")==0) return 0x1f5;
        if(strcmp(key,"KEY_BRL_DOT6")==0) return 0x1f6;
        if(strcmp(key,"KEY_BRL_DOT7")==0) return 0x1f7;
        if(strcmp(key,"KEY_BRL_DOT8")==0) return 0x1f8;
        if(strcmp(key,"KEY_BRL_DOT9")==0) return 0x1f9;
        if(strcmp(key,"KEY_BRL_DOT10")==0) return 0x1fa;

        if(strcmp(key,"KEY_NUMERIC_0")==0) return 0x200	/* used by phones, remote controls, */;
        if(strcmp(key,"KEY_NUMERIC_1")==0) return 0x201	/* and other keypads */;
        if(strcmp(key,"KEY_NUMERIC_2")==0) return 0x202;
        if(strcmp(key,"KEY_NUMERIC_3")==0) return 0x203;
        if(strcmp(key,"KEY_NUMERIC_4")==0) return 0x204;
        if(strcmp(key,"KEY_NUMERIC_5")==0) return 0x205;
        if(strcmp(key,"KEY_NUMERIC_6")==0) return 0x206;
        if(strcmp(key,"KEY_NUMERIC_7")==0) return 0x207;
        if(strcmp(key,"KEY_NUMERIC_8")==0) return 0x208;
        if(strcmp(key,"KEY_NUMERIC_9")==0) return 0x209;
        if(strcmp(key,"KEY_NUMERIC_STAR")==0)	0x20a;
        if(strcmp(key,"KEY_NUMERIC_POUND")==0)	0x20b;

        if(strcmp(key,"KEY_CAMERA_FOCUS")==0)	0x210;
        if(strcmp(key,"KEY_WPS_BUTTON")==0) return 0x211	/* WiFi Protected Setup key */;

        if(strcmp(key,"KEY_TOUCHPAD_TOGGLE")==0)	0x212	/* Request switch touchpad on or off */;
        if(strcmp(key,"KEY_TOUCHPAD_ON")==0) return 0x213;
        if(strcmp(key,"KEY_TOUCHPAD_OFF")==0)	0x214;

        if(strcmp(key,"KEY_CAMERA_ZOOMIN")==0)	0x215;
        if(strcmp(key,"KEY_CAMERA_ZOOMOUT")==0)	0x216;
        if(strcmp(key,"KEY_CAMERA_UP")==0) return 0x217;
        if(strcmp(key,"KEY_CAMERA_DOWN")==0) return 0x218;
        if(strcmp(key,"KEY_CAMERA_LEFT")==0) return 0x219;
        if(strcmp(key,"KEY_CAMERA_RIGHT")==0)	0x21a;

        if(strcmp(key,"BTN_TRIGGER_HAPPY")==0) return 0x2c0;
        if(strcmp(key,"BTN_TRIGGER_HAPPY1")==0) return 0x2c0;
        if(strcmp(key,"BTN_TRIGGER_HAPPY2")==0) return 0x2c1;
        if(strcmp(key,"BTN_TRIGGER_HAPPY3")==0) return 0x2c2;
        if(strcmp(key,"BTN_TRIGGER_HAPPY4")==0) return 0x2c3;
        if(strcmp(key,"BTN_TRIGGER_HAPPY5")==0) return 0x2c4;
        if(strcmp(key,"BTN_TRIGGER_HAPPY6")==0) return 0x2c5;
        if(strcmp(key,"BTN_TRIGGER_HAPPY7")==0) return 0x2c6;
        if(strcmp(key,"BTN_TRIGGER_HAPPY8")==0) return 0x2c7;
        if(strcmp(key,"BTN_TRIGGER_HAPPY9")==0) return 0x2c8;
        if(strcmp(key,"BTN_TRIGGER_HAPPY10")==0) return 0x2c9;
        if(strcmp(key,"BTN_TRIGGER_HAPPY11")==0) return 0x2ca;
        if(strcmp(key,"BTN_TRIGGER_HAPPY12")==0) return 0x2cb;
        if(strcmp(key,"BTN_TRIGGER_HAPPY13")==0) return 0x2cc;
        if(strcmp(key,"BTN_TRIGGER_HAPPY14")==0) return 0x2cd;
        if(strcmp(key,"BTN_TRIGGER_HAPPY15")==0) return 0x2ce;
        if(strcmp(key,"BTN_TRIGGER_HAPPY16")==0) return 0x2cf;
        if(strcmp(key,"BTN_TRIGGER_HAPPY17")==0) return 0x2d0;
        if(strcmp(key,"BTN_TRIGGER_HAPPY18")==0) return 0x2d1;
        if(strcmp(key,"BTN_TRIGGER_HAPPY19")==0) return 0x2d2;
        if(strcmp(key,"BTN_TRIGGER_HAPPY20")==0) return 0x2d3;
        if(strcmp(key,"BTN_TRIGGER_HAPPY21")==0) return 0x2d4;
        if(strcmp(key,"BTN_TRIGGER_HAPPY22")==0) return 0x2d5;
        if(strcmp(key,"BTN_TRIGGER_HAPPY23")==0) return 0x2d6;
        if(strcmp(key,"BTN_TRIGGER_HAPPY24")==0) return 0x2d7;
        if(strcmp(key,"BTN_TRIGGER_HAPPY25")==0) return 0x2d8;
        if(strcmp(key,"BTN_TRIGGER_HAPPY26")==0) return 0x2d9;
        if(strcmp(key,"BTN_TRIGGER_HAPPY27")==0) return 0x2da;
        if(strcmp(key,"BTN_TRIGGER_HAPPY28")==0) return 0x2db;
        if(strcmp(key,"BTN_TRIGGER_HAPPY29")==0) return 0x2dc;
        if(strcmp(key,"BTN_TRIGGER_HAPPY30")==0) return 0x2dd;
        if(strcmp(key,"BTN_TRIGGER_HAPPY31")==0) return 0x2de;
        if(strcmp(key,"BTN_TRIGGER_HAPPY32")==0) return 0x2df;
        if(strcmp(key,"BTN_TRIGGER_HAPPY33")==0) return 0x2e0;
        if(strcmp(key,"BTN_TRIGGER_HAPPY34")==0) return 0x2e1;
        if(strcmp(key,"BTN_TRIGGER_HAPPY35")==0) return 0x2e2;
        if(strcmp(key,"BTN_TRIGGER_HAPPY36")==0) return 0x2e3;
        if(strcmp(key,"BTN_TRIGGER_HAPPY37")==0) return 0x2e4;
        if(strcmp(key,"BTN_TRIGGER_HAPPY38")==0) return 0x2e5;
        if(strcmp(key,"BTN_TRIGGER_HAPPY39")==0) return 0x2e6;
        if(strcmp(key,"BTN_TRIGGER_HAPPY40")==0) return 0x2e7;

        /* We avoid low common keys in module aliases so they don"t get huge. */
        if(strcmp(key,"KEY_MIN_INTERESTING")==0)	KEY_MUTE;
        if(strcmp(key,"KEY_MAX")==0) return 	0x2ff;
        if(strcmp(key,"KEY_CNT")==0) return 	(KEY_MAX+1);

        /*
         * Relative axes
         */

        if(strcmp(key,"REL_X")==0) return 	0x00;
        if(strcmp(key,"REL_Y")==0) return 	0x01;
        if(strcmp(key,"REL_Z")==0) return 	0x02;
        if(strcmp(key,"REL_RX")==0) return 	0x03;
        if(strcmp(key,"REL_RY")==0) return 	0x04;
        if(strcmp(key,"REL_RZ")==0) return 	0x05;
        if(strcmp(key,"REL_HWHEEL")==0) return 0x06;
        if(strcmp(key,"REL_DIAL")==0) return 0x07;
        if(strcmp(key,"REL_WHEEL")==0) return 0x08;
        if(strcmp(key,"REL_MISC")==0) return 0x09;
        if(strcmp(key,"REL_MAX")==0) return 	0x0f;
        if(strcmp(key,"REL_CNT")==0) return 	(REL_MAX+1);

        /*
         * Absolute axes
         */

        if(strcmp(key,"ABS_X")==0) return 	0x00;
        if(strcmp(key,"ABS_Y")==0) return 	0x01;
        if(strcmp(key,"ABS_Z")==0) return 	0x02;
        if(strcmp(key,"ABS_RX")==0) return 	0x03;
        if(strcmp(key,"ABS_RY")==0) return 	0x04;
        if(strcmp(key,"ABS_RZ")==0) return 	0x05;
        if(strcmp(key,"ABS_THROTTLE")==0) return 0x06;
        if(strcmp(key,"ABS_RUDDER")==0) return 0x07;
        if(strcmp(key,"ABS_WHEEL")==0) return 0x08;
        if(strcmp(key,"ABS_GAS")==0) return 	0x09;
        if(strcmp(key,"ABS_BRAKE")==0) return 0x0a;
        if(strcmp(key,"ABS_HAT0X")==0) return 0x10;
        if(strcmp(key,"ABS_HAT0Y")==0) return 0x11;
        if(strcmp(key,"ABS_HAT1X")==0) return 0x12;
        if(strcmp(key,"ABS_HAT1Y")==0) return 0x13;
        if(strcmp(key,"ABS_HAT2X")==0) return 0x14;
        if(strcmp(key,"ABS_HAT2Y")==0) return 0x15;
        if(strcmp(key,"ABS_HAT3X")==0) return 0x16;
        if(strcmp(key,"ABS_HAT3Y")==0) return 0x17;
        if(strcmp(key,"ABS_PRESSURE")==0) return 0x18;
        if(strcmp(key,"ABS_DISTANCE")==0) return 0x19;
        if(strcmp(key,"ABS_TILT_X")==0) return 0x1a;
        if(strcmp(key,"ABS_TILT_Y")==0) return 0x1b;
        if(strcmp(key,"ABS_TOOL_WIDTH")==0) return 0x1c;

        if(strcmp(key,"ABS_VOLUME")==0) return 0x20;

        if(strcmp(key,"ABS_MISC")==0) return 0x28;

        if(strcmp(key,"ABS_MT_SLOT")==0) return 0x2f	/* MT slot being modified */;
        if(strcmp(key,"ABS_MT_TOUCH_MAJOR")==0)	0x30	/* Major axis of touching ellipse */;
        if(strcmp(key,"ABS_MT_TOUCH_MINOR")==0)	0x31	/* Minor axis (omit if circular) */;
        if(strcmp(key,"ABS_MT_WIDTH_MAJOR")==0)	0x32	/* Major axis of approaching ellipse */;
        if(strcmp(key,"ABS_MT_WIDTH_MINOR")==0)	0x33	/* Minor axis (omit if circular) */;
        if(strcmp(key,"ABS_MT_ORIENTATION")==0)	0x34	/* Ellipse orientation */;
        if(strcmp(key,"ABS_MT_POSITION_X")==0)	0x35	/* Center X ellipse position */;
        if(strcmp(key,"ABS_MT_POSITION_Y")==0)	0x36	/* Center Y ellipse position */;
        if(strcmp(key,"ABS_MT_TOOL_TYPE")==0)	0x37	/* Type of touching device */;
        if(strcmp(key,"ABS_MT_BLOB_ID")==0) return 0x38	/* Group a set of packets as a blob */;
        if(strcmp(key,"ABS_MT_TRACKING_ID")==0)	0x39	/* Unique ID of initiated contact */;
        if(strcmp(key,"ABS_MT_PRESSURE")==0) return 0x3a	/* Pressure on contact area */;
        if(strcmp(key,"ABS_MT_DISTANCE")==0) return 0x3b	/* Contact hover distance */;

        if(strcmp(key,"ABS_MAX")==0) return 	0x3f;
        
        
        return -1;
        

}

int resolveGKeyValue(char *key) {
 
    if(strcmp(key,"G1")==0) return 1;
    if(strcmp(key,"G2")==0) return 2;
    if(strcmp(key,"G3")==0) return 3;
    if(strcmp(key,"G4")==0) return 4;
    if(strcmp(key,"G5")==0) return 5;
    if(strcmp(key,"G6")==0) return 6;
    if(strcmp(key,"G7")==0) return 7;
    if(strcmp(key,"G8")==0) return 8;
    if(strcmp(key,"G9")==0) return 9;
    if(strcmp(key,"G10")==0) return 10;
    if(strcmp(key,"G11")==0) return 11;
    if(strcmp(key,"G12")==0) return 12;
    if(strcmp(key,"G13")==0) return 13;
    if(strcmp(key,"G14")==0) return 14;
    if(strcmp(key,"G15")==0) return 15;
    if(strcmp(key,"G16")==0) return 16;
    if(strcmp(key,"G17")==0) return 17;
    if(strcmp(key,"G18")==0) return 18;
    if(strcmp(key,"G19")==0) return 19;
    if(strcmp(key,"G20")==0) return 20;
    if(strcmp(key,"G21")==0) return 21;
    if(strcmp(key,"G22")==0) return 22;
    
    if(strcmp(key,"G23")==0) return 23;
    if(strcmp(key,"G24")==0) return 24;
    if(strcmp(key,"G25")==0) return 25;
    
    return -1;
    
}

int resolveAxis(char *key) {
 
    if(strcmp(key,"JOY_LEFT")==0) return G13_XAXIS;
    if(strcmp(key,"JOY_RIGHT")==0) return G13_XAXIS;
    if(strcmp(key,"JOY_UP")==0) return G13_YAXIS;
    if(strcmp(key,"JOY_DOWN")==0) return G13_YAXIS;
        
    return -1;
    
}

int resolveDir(char *key) {
 
    if(strcmp(key,"JOY_LEFT")==0) return G13_NEGATIVE;
    if(strcmp(key,"JOY_RIGHT")==0) return G13_POSITIVE;
    if(strcmp(key,"JOY_UP")==0) return G13_NEGATIVE;
    if(strcmp(key,"JOY_DOWN")==0) return G13_POSITIVE;
        
    return -1;
    
}