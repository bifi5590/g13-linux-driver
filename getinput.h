/* 
 * File:   getinput.h
 * Author: bifi
 *
 * Created on 12. Februar 2013, 21:36
 */

#ifndef GETINPUT_H
#define	GETINPUT_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "g13constants.h"
#include "logic_define.h"

void getPressedKeys(unsigned char *data) {
    unsigned char x = data[3];
    //Keys G1 - G8
    if((x & G13_G1) == G13_G1) {
        if(verbose) printf("G1 ");
        gKeyData[1] = true;
    }else{
        gKeyData[1] = false;
    }
    if((x & G13_G2) == G13_G2) {
        if(verbose) printf("G2 ");
        gKeyData[2] = true;
    }else{
        gKeyData[2] = false;
    }
    if((x & G13_G3) == G13_G3) {
        if(verbose) printf("G3 ");
        gKeyData[3] = true;
    }else{
        gKeyData[3] = false;
    }
    if((x & G13_G4) == G13_G4) {
        if(verbose) printf("G4 ");
        gKeyData[4] = true;
    }else{
        gKeyData[4] = false;
    }
    if((x & G13_G5) == G13_G5) {
        if(verbose) printf("G5 ");
        gKeyData[5] = true;
    }else{
        gKeyData[5] = false;
    }
    if((x & G13_G6) == G13_G6) {
        if(verbose) printf("G6 ");
        gKeyData[6] = true;
    }else{
        gKeyData[6] = false;
    }
    if((x & G13_G7) == G13_G7) {
        if(verbose) printf("G7 ");
        gKeyData[7] = true;
    }else{
        gKeyData[7] = false;
    }
    if((x & G13_G8) == G13_G8) {
        if(verbose) printf("G8 ");
        gKeyData[8] = true;
    }else{
        gKeyData[8] = false;
    }

    //G9 - G16
    x = data[4];
    if((x & G13_G9) == G13_G9) {
        if(verbose) printf("G9 ");
        gKeyData[9] = true;
    }else{
        gKeyData[9] = false;
    }
    if((x & G13_G10) == G13_G10) {
        if(verbose) printf("G10 ");
        gKeyData[10] = true;
    }else{
        gKeyData[10] = false;
    }
    if((x & G13_G11) == G13_G11) {
        if(verbose) printf("G11 ");
        gKeyData[11] = true;
    }else{
        gKeyData[11] = false;
    }
    if((x & G13_G12) == G13_G12) {
        if(verbose) printf("G12 ");
        gKeyData[12] = true;
    }else{
        gKeyData[12] = false;
    }
    if((x & G13_G13) == G13_G13) {
        if(verbose) printf("G13 ");
        gKeyData[13] = true;
    }else{
        gKeyData[13] = false;
    }
    if((x & G13_G14) == G13_G14) {
        if(verbose) printf("G14 ");
        gKeyData[14] = true;
    }else{
        gKeyData[14] = false;
    }
    if((x & G13_G15) == G13_G15) {
        if(verbose) printf("G15 ");
        gKeyData[15] = true;
    }else{
        gKeyData[15] = false;
    }
    if((x & G13_G16) == G13_G16) {
        if(verbose) printf("G16 ");
        gKeyData[16] = true;
    }else{
        gKeyData[16] = false;
    }

    //G17 - G22
    x = data[5];
    if((x & G13_G17) == G13_G17) {
        if(verbose) printf("G17 ");
        gKeyData[17] = true;
    }else{
        gKeyData[17] = false;
    }
    if((x & G13_G18) == G13_G18) {
        if(verbose) printf("G18 ");
        gKeyData[18] = true;
    }else{
        gKeyData[18] = false;
    }
    if((x & G13_G19) == G13_G19) {
        if(verbose) printf("G19 ");
        gKeyData[19] = true;
    }else{
        gKeyData[19] = false;
    }
    if((x & G13_G20) == G13_G20) {
        if(verbose) printf("G20 ");
        gKeyData[20] = true;
    }else{
        gKeyData[20] = false;
    }
    if((x & G13_G21) == G13_G21) {
        if(verbose) printf("G21 ");
        gKeyData[21] = true;
    }else{
        gKeyData[21] = false;
    }
    if((x & G13_G22) == G13_G22) {
        if(verbose) printf("G22 ");
        gKeyData[22] = true;
    }else{
        gKeyData[22] = false;
    }
    
    //G23 - G25
    x = data[7];
    if((x & G13_G23) == G13_G23) {
        if(verbose) printf("G23 ");
        gKeyData[23] = true;
    }else{
        gKeyData[23] = false;
    }
    if((x & G13_G24) == G13_G24) {
        if(verbose) printf("G24 ");
        gKeyData[24] = true;
    }else{
        gKeyData[24] = false;
    }
    if((x & G13_G25) == G13_G25) {
        if(verbose) printf("G25 ");
        gKeyData[25] = true;
    }else{
        gKeyData[25] = false;
    }
    
    //M1 - M3
    x = data[6];
    if((x & G13_M1) == G13_M1) {
        if(verbose) printf("M1 ");
        mKeyData[1] = true;
    }else{
        mKeyData[1] = false;
    }
    if((x & G13_M2) == G13_M2) {
        if(verbose) printf("M2 ");
        mKeyData[2] = true;
    }else{
        mKeyData[2] = false;
    }
    if((x & G13_M3) == G13_M3) {
        if(verbose) printf("M3 ");
        mKeyData[3] = true;
    }else{
        mKeyData[3] = false;
    }

    x = data[7];
    if((x & G13_MR) == G13_MR) {
        if(verbose) printf("MR ");
        mKeyData[4] = true;
    }else{
        mKeyData[4] = false;
    }
    
    //P1-P5
    x = data[6];
    if((x & G13_P1) == G13_P1) {
        if(verbose) printf("P1 ");
        pKeyData[1] = true;
    }else{
        pKeyData[1] = false;
    }
    if((x & G13_P2) == G13_P2) {
        if(verbose) printf("P2 ");
        pKeyData[2] = true;
    }else{
        pKeyData[2] = false;
    }
    if((x & G13_P3) == G13_P3) {
        if(verbose) printf("P3 ");
        pKeyData[3] = true;
    }else{
        pKeyData[3] = false;
    }
    if((x & G13_P4) == G13_P4) {
        if(verbose) printf("P4 ");
        pKeyData[4] = true;
    }else{
        pKeyData[4] = false;
    }
    if((x & G13_P5) == G13_P5) {
        if(verbose) printf("P5 ");
        pKeyData[5] = true;
    }else{
        pKeyData[5] = false;
    }
    
    //if(verbose) printf("\n"); //ARF!
}

void getJoystick(unsigned char *data) {
    
    int x_axis = data[1];
    int y_axis = data[2];
    
    if(x_axis > G13_JOYSTICK_XCENTER + G13_JOYSTICK_XTOLERANCE) {
        joyData[G13_XAXIS][G13_POSITIVE] = true;
        joyData[G13_XAXIS][G13_NEGATIVE] = false;
        if(verbose) printf("JOY_RIGHT, ");
    }else if(x_axis < G13_JOYSTICK_XCENTER - G13_JOYSTICK_XTOLERANCE) {
        joyData[G13_XAXIS][G13_POSITIVE] = false;
        joyData[G13_XAXIS][G13_NEGATIVE] = true;
        if(verbose) printf("JOY_LEFT, ");
    }else{
        joyData[G13_XAXIS][G13_POSITIVE] = false;
        joyData[G13_XAXIS][G13_NEGATIVE] = false;
    }
    
    if(y_axis > G13_JOYSTICK_YCENTER + G13_JOYSTICK_YTOLERANCE) {
        joyData[G13_YAXIS][G13_POSITIVE] = true;
        joyData[G13_YAXIS][G13_NEGATIVE] = false;
        if(verbose) printf("JOY_DOWN, ");
    }else if(y_axis < G13_JOYSTICK_YCENTER - G13_JOYSTICK_YTOLERANCE) {
        joyData[G13_YAXIS][G13_POSITIVE] = false;
        joyData[G13_YAXIS][G13_NEGATIVE] = true;
        if(verbose) printf("JOY_UP, ");
    }else{
        joyData[G13_YAXIS][G13_POSITIVE] = false;
        joyData[G13_YAXIS][G13_NEGATIVE] = false;
    }
    if(verbose) printf("\n");
    
    for(int axis=0;axis<=1;axis++) {
        for(int dir=0; dir<=1;dir++) {
            memset(&ev, 0, sizeof(struct input_event));
            gettimeofday(&ev.time, 0);
            ev.type = EV_KEY;
            if(joyData[axis][dir] == true) {
                ev.value = KEY_PRESS;
            }else{
                ev.value = KEY_RELEASE;
            }

            ev.code = joyBindings[activeM][axis][dir];

            if(ev.code > 0) {
                if(write(fd, &ev, sizeof(struct input_event)) != sizeof(struct input_event)) {
                    if(verbose) printf("Unable to send Joystick key Stroke.\n");
                }
            }
        }
    }
    
    //Sync Event:
    memset(&ev, 0, sizeof(struct input_event));
    ev.type = EV_SYN;
    ev.code = SYN_REPORT;
    ev.value = 0;
    write(fd, &ev, sizeof(struct input_event));
    
    
}


#ifdef	__cplusplus
}
#endif

#endif	/* GETINPUT_H */

