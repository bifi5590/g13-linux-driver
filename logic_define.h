/* 
 * File:   logic_define.h
 * Author: bifi
 *
 * Created on 26. Dezember 2012, 13:51
 */

#ifndef LOGIC_DEFINE_H
#define	LOGIC_DEFINE_H

#ifdef	__cplusplus
extern "C" {
#endif


#define true 1
#define false 0
#define bool unsigned char
#define AND &&
#define OR ||
    
#define KEY_PRESS 1
#define KEY_RELEASE 0
    

#ifdef	__cplusplus
}
#endif

#endif	/* LOGIC_DEFINE_H */

