/* 
 * File:   g13constants.h
 * Author: bifi
 *
 * Created on 24. Dezember 2012, 15:47
 */

#ifndef G13CONSTANTS_H
#define	G13CONSTANTS_H

#ifdef	__cplusplus
extern "C" {
#endif
    

#define G13_KEY_ENDPOINT 1
#define G13_LCD_ENDPOINT 2
#define G13_REPORT_SIZE 8
#define G13_LCD_BUFFER_SIZE 0x3c0
    
#define G13_VENDOR 0x046d
#define G13_PRODUCT 0xc21c
#define G13_GKEYS 25
#define G13_MKEYS 4
#define G13_PKEYS 5
#define G13_JOYSTICK_AXES 2
    
#define G13_XAXIS 0
#define G13_YAXIS 1    
#define G13_JOYSTICK_XCENTER 0x7F
#define G13_JOYSTICK_YCENTER 0x7F
#define G13_JOYSTICK_XTOLERANCE 0x20
#define G13_JOYSTICK_YTOLERANCE 0x20
#define G13_POSITIVE 1
#define G13_NEGATIVE 0

#define G13_G1 0x1
#define G13_G2 0x2
#define G13_G3 0x4
#define G13_G4 0x8
#define G13_G5 0x10
#define G13_G6 0x20
#define G13_G7 0x40
#define G13_G8 0x80

#define G13_G9 0x1
#define G13_G10 0x2
#define G13_G11 0x4
#define G13_G12 0x8
#define G13_G13 0x10
#define G13_G14 0x20
#define G13_G15 0x40
#define G13_G16 0x80

#define G13_G17 0x1
#define G13_G18 0x2
#define G13_G19 0x4
#define G13_G20 0x8
#define G13_G21 0x10
#define G13_G22 0x20
    
#define G13_G23 0x2
#define G13_G24 0x4
#define G13_G25 0x8
    
#define G13_M1 0x20
#define G13_M2 0x40
#define G13_M3 0x80
#define G13_MR 0x1
    
#define G13_P1 0x1
#define G13_P2 0x2
#define G13_P3 0x4
#define G13_P4 0x8
#define G13_P5 0x10
    

#ifdef	__cplusplus
}
#endif

#endif	/* G13CONSTANTS_H */

