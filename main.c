/* 
 * File:   main.c
 * Author: Alexander von Birgelen
 * Version: see git
 *
 * Created on 24. Dezember 2012, 10:35
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <iostream>
#include <math.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/input.h>
#include <linux/uinput.h>
#include <string.h>

#include <libusb-1.0/libusb.h>

#include "globalvariable.h"
#include "g13constants.h"
#include "logic_define.h"
#include "getinput.h"
#include "keys.h"

//void getPressedKeys(unsigned char *data);
//void getJoystick(unsigned char *data);
void performKeyStrokes();
void setupuinput();
void setupusbdevice();
void initKeyBinding(char *file);
void setColor(int red, int green, int blue);
void setModeLeds(int leds);
void setLogo(char *file);
void runProg(int i);

int main(int argc, char** argv) {
    
    unsigned char data[G13_REPORT_SIZE];
    int actual_length;
    bool run = true;   
    int r;
    int filearg = 1;
    
    if(argc == 0) {
        printf("No config file.\n");
        return 1;
    }
    
    for(int i=0;i<argc;i++) {
        
        if( (strcmp(argv[i],"-v")==0) ) {
            verbose = 1;
        }
        
        if( (strcmp(argv[i],"-b")==0) ) {
            verbosebuffer = 1;
        }
        
        if( (strcmp(argv[i],"-f")==0) AND (argc > i) ) {
            filearg = i+1;
        }
        
    }
    
    system("rm ./nohup.out");
    
    for(int j=0; j<G13_MKEYS;j++) {
        for(int i=0;i<=G13_GKEYS;i++) {
            keyBindings[j][i] = -1;
        }
        
        joyBindings[j][0][0] = -1; joyBindings[j][0][1] = -1;
        joyBindings[j][1][0] = -1; joyBindings[j][1][1] = -1;
        
    }
    
    joyData[0][0] = false; joyData[0][1] = false;
    joyData[1][0] = false; joyData[1][1] = false;
    
    for(int i=0;i<=G13_GKEYS;i++) {
        gKeyData[i] = false;
        old_gKeyData[i] = false;
    }
    
    memset(&prog, 0, sizeof(prog));
    
    setupusbdevice();
    setupuinput();
    initKeyBinding(argv[filearg]);

    printf("Waiting for inputs...\n");
    
    do {
        r = libusb_interrupt_transfer(dev_handle, LIBUSB_ENDPOINT_IN | G13_KEY_ENDPOINT, data, sizeof(data), &actual_length, 0);
        
        if (r == 0 && actual_length == sizeof(data)) {
            
            if(verbosebuffer) {
                printf("Buffer: "); 
                for(int i=1; i<8; i++) { 
                    printf("%X-",data[i]); 
                }
                printf("\n");
            }
            
            getPressedKeys(data);
            getJoystick(data);
            performKeyStrokes();
        }else{
            printf("Error: %d, Size: %d\n", r, actual_length);
        }

        if(r < 0) {
            printf("error: %d\n", r);
            run=false;
        }
    
    }while(run == true);
    
    ioctl(fd, UI_DEV_DESTROY);    
    close(fd);
    libusb_release_interface(dev_handle,0);
    libusb_close(dev_handle);
    libusb_exit(ctx);
    printf("Goodbye!");
    
    return (EXIT_SUCCESS);
}

void setupuinput() {
    int ret;
    
    if(access("/dev/uinput", W_OK) != 0) {
      printf("/dev/uinput doesn't grant write permissions\n");
      exit(EXIT_FAILURE);
    }
    
    fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK/* | O_NDELAY*/);
    if(fd < 0) {
        printf("Error opening uinput: %d\n", fd);
        return;
    }
    
    memset(&uidev, 0, sizeof(uidev));
    char name[] = "G13";
    strncpy(uidev.name, name, sizeof(name));
    uidev.id.bustype = BUS_USB;
    uidev.id.vendor = G13_VENDOR;
    uidev.id.product = G13_PRODUCT;
    uidev.id.version = 1;
  
    
    ret = write(fd, &uidev, sizeof(struct uinput_user_dev));
    if(ret < 0) {
        printf("Unable to write Info\n");
        return;
    }
    
    //allow key press / release:
    ret = ioctl(fd, UI_SET_EVBIT, EV_KEY);
    if(ret < 0) {
        printf("Unable to allow key presses.");
        return;
    }

    for(int i=0; i<256; i++) {
        ret = ioctl(fd, UI_SET_KEYBIT, i);
        if(ret < 0) {
            printf("Unable to enable Key %d\n", i);
            return;
        }
    }
    
    ret = ioctl(fd, UI_SET_EVBIT, EV_SYN);
    if(ret < 0) {
        printf("Unable to allow synchro.");
        return;
    }
    
    
    ret = ioctl(fd, UI_DEV_CREATE);
    if(ret < 0) {
        printf("Unable to create device\n");
        return;
    }
    
    sleep(1);
    printf("Uinput device created!\n");
    
}

void performKeyStrokes() {
    
    for(int i=1; i<=G13_MKEYS; i++) {
        if(mKeyData[i] == true) {
            activeM = i;
            setModeLeds(i);
            if(verbose) printf("M%d active.\n",i);
        }
    }
    
    for(int i=1; i<=G13_PKEYS; i++) {
        if(pKeyData[i] == true) {
            
            runProg(i);
            
            if(verbose) printf("P%d pressed.\n",i);
        }
    }
    
    for(int i=1; i<=G13_GKEYS;i++) { //loop through all G keys
        if(gKeyData[i] != old_gKeyData[i]) {
            memset(&ev, 0, sizeof(struct input_event));
            
            gettimeofday(&ev.time, 0);
            
            ev.type = EV_KEY;
            
            if(gKeyData[i] == true) {
                ev.value = KEY_PRESS;
            }else{
                ev.value = KEY_RELEASE;
            }
            
            ev.code = keyBindings[activeM][i];
            
            if(ev.code > 0) {
                if(write(fd, &ev, sizeof(struct input_event)) != sizeof(struct input_event)) {
                    printf("Unable to send key Stroke.\n");
                }
            }

            old_gKeyData[i] = gKeyData[i];
            
        }
    }
    
    //Sync Event:
    memset(&ev, 0, sizeof(struct input_event));
    ev.type = EV_SYN;
    ev.code = SYN_REPORT;
    ev.value = 0;
    write(fd, &ev, sizeof(struct input_event));
    
}

void setupusbdevice() {
    libusb_device **devices; //pointer to pointer to device
    ssize_t cnt; //number of devices
    int r;

    r = libusb_init(&ctx);
    if(r <0) {
        printf("Init failed with %d\n",r);
        return;
    }
    
    libusb_set_debug(ctx,3); //set debug level
    
    cnt = libusb_get_device_list(ctx, &devices);
    if(cnt < 0) {
        printf("Get device error\n");
        //return 1;
    }
    
    printf("There are %d devices in list. Trying to get G13...\n", (int)cnt);
    
    
    dev_handle = libusb_open_device_with_vid_pid(ctx, G13_VENDOR, G13_PRODUCT);
    
    if(dev_handle == NULL) {
        printf("No G13 device found!\n");
    }else{
        printf("Found a G13!\n");
    }
    
    libusb_free_device_list(devices, 1); //free list
    
    
    if(libusb_kernel_driver_active(dev_handle,0) == 1) {
        printf("Kernel driver active. Detaching...");
        if(libusb_detach_kernel_driver(dev_handle,0) == 0) {
            printf("success!\n");
        }else{
            printf("failed! :(\n");
        }
    }else{
        printf("No kernel driver active!\n");
    }
    
    r = libusb_claim_interface(dev_handle,0);
    if(r < 0) {
        printf("Can not claim interface.");
        return;
    }
    
    printf("Interface claimed!\n");
}

void initKeyBinding(char *file) {
    char gkey[10];
    int g;
    int axis, dir;
    int k;
    int p;
    int red, green, blue;
    char key[40];
    char lfile[255];
    FILE *fh;
    
    printf("Reading file %s...\n",file);
    
    fh = fopen(file, "r");
    if(fh < 0) {
        printf("Can not open file.");
        return;
    }
    
    while(fscanf(fh, "%s", gkey) != EOF) {
        
        if(strcmp(gkey,"RGB")==0) {
            fscanf(fh, "%d", &red);
            fscanf(fh, "%d", &green);
            fscanf(fh, "%d", &blue);
            
            setColor(red,green,blue);
        }else if(strcmp(gkey,"LOGO")==0) {
            
            fscanf(fh, "%s", lfile);
            setLogo(lfile);
        
        }else if( (strcmp(gkey,"JOY_LEFT")==0) OR (strcmp(gkey,"JOY_RIGHT")==0) OR (strcmp(gkey,"JOY_UP")==0) OR (strcmp(gkey,"JOY_DOWN")==0)) {
            fscanf(fh, "%s", key);
            printf("Map %s to %s.\n",gkey,key);
            
            axis = resolveAxis(gkey);
            dir = resolveDir(gkey);
            k = resolveKeyValue(key);
            
            if(axis != -1 AND dir != -1 AND k != -1) {
                joyBindings[activeM][axis][dir] = k;
            }else{
                printf("Joystick Keybinding error detectet.\n");
            }
        }else if( (strcmp(gkey,"M1")==0) ) {
            activeM=1;
            printf("Mapping to M%d:\n", activeM);
        }else if( (strcmp(gkey,"M2")==0) ) {
            activeM=2;
            printf("Mapping to M%d:\n", activeM);
        }else if( (strcmp(gkey,"M3")==0) ) {
            activeM=3;
            printf("Mapping to M%d:\n", activeM);
        }else if( (strcmp(gkey,"MR")==0) ) {
            activeM=4;
            printf("Mapping to M%d:\n", activeM);
            
            
            
        }else if( (strcmp(gkey,"P1")==0) ) {
            int p=1;
            fscanf(fh, "%s", &prog[p]);
            printf("Mapping to P%d: %s\n", p, prog[p]);
        }else if( (strcmp(gkey,"P2")==0) ) {
            int p=2;
            fscanf(fh, "%s", &prog[p]);
            printf("Mapping to P%d: %s\n", p, prog[p]);
        }else if( (strcmp(gkey,"P3")==0) ) {
            int p=3;
            fscanf(fh, "%s", &prog[p]);
            printf("Mapping to P%d: %s\n", p, prog[p]);
        }else if( (strcmp(gkey,"P4")==0) ) {
            int p=4;
            fscanf(fh, "%s", &prog[p]);
            printf("Mapping to P%d: %s\n", p, prog[p]);
        }else if( (strcmp(gkey,"P5")==0) ) {
            int p=5;
            fscanf(fh, "%s", &prog[p]);
            printf("Mapping to P%d: %s\n", p, prog[p]);


            
        }else{
            fscanf(fh, "%s", key);
            printf("Map %s to %s.\n",gkey,key);
            g = resolveGKeyValue(gkey);
            k = resolveKeyValue(key);

            if(g != -1 AND k != -1) {
                    keyBindings[activeM][g] = k;   
            }else{
                printf("Keybinding error detectet.\n");
            }
        }
    }
    
    setModeLeds(activeM);
    
    fclose(fh);
    
}

void setColor(int red, int green, int blue) {
    int e;
    unsigned char usb_data[] = { 5, 0, 0, 0, 0 };
    usb_data[1] = red;
    usb_data[2] = green;
    usb_data[3] = blue;
    
    e = libusb_control_transfer(dev_handle, LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE, 9, 0x307, 0, usb_data, 5, 1000);
    if(e!=5) {
        printf("Error setting color.");
    }
    
}

void setModeLeds(int leds) {
    int e, val;
    unsigned char usb_data[] = { 5, 0, 0, 0, 0 };
    
    if(leds>=1 AND leds<=4) {
        val = (int) pow(2,leds-1);
    }else{
        val=0;
    }

    usb_data[1] = val;

    e = libusb_control_transfer(dev_handle, LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE, 9, 0x305, 0, usb_data, 5, 1000);
    if(e!=5) {
        printf("Error setting color.");
    }
    
}

void setLogo(char *file) {
    
    unsigned char buffer[G13_LCD_BUFFER_SIZE+32];
    unsigned char *pbuffer;
    int i=0;
    int c=0;
    int error;
    int bytes;
    
    memset(buffer, 0, G13_LCD_BUFFER_SIZE + 32);
    buffer[0] = 0x03;
    pbuffer = buffer+32;
    
    printf("Loading Image %s.\n",file);
    
    FILE *fh = fopen(file,"r");
    
    //init lcd:
    error = libusb_control_transfer(dev_handle, 0, 9, 1, 0, NULL, 0, 1000);
    if(error) {
        printf("Could not init LCD");
    }
    
    while(EOF != (c=fgetc(fh))) {
        *pbuffer = c;
        pbuffer++;
        i++;
    }
    
    fclose(fh);
    
    error = libusb_interrupt_transfer(dev_handle, LIBUSB_ENDPOINT_OUT | G13_LCD_ENDPOINT, buffer, G13_LCD_BUFFER_SIZE + 32, &bytes, 1000);
    if(error) {
        printf("Error when transferring Image.");
        return;
    }
    
    printf("Logo added.\n");
    
}

void runProg(int i) {
    
    char cmd[200] = "/bin/sh -c \" nohup ";
    char end[5] = " \" &";
    //char prg[100] = "/home/bifi/prog_install/Slic3r/bin/slic3r";
    if(strlen(prog[i])>0) {
        strcat(cmd, prog[i]);
        strcat(cmd, end);        
        system(cmd);
    }
    //system("/bin/sh -c \"/home/bifi/prog_install/Slic3r/bin/slic3r\" &");
    
}