#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <linux/input.h>
#include <linux/uinput.h>

#define G13_VENDOR 0x046d
#define G13_PRODUCT 0xc21c


int fd;
struct uinput_user_dev uidev;
struct input_event ev;

int main() {

	int ret;
    
    if(access("/dev/uinput", W_OK) != 0) {
      printf("/dev/uinput doesn't grant write permissions\n");
      return 0;
    }
    
    fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK/* | O_NDELAY*/);
    if(fd < 0) {
        printf("Error opening uinput: %d\n", fd);
        return 0;
    }
    
    //printf("fd: %d", fd);
    
    memset(&uidev, 0, sizeof(uidev));
    char name[] = "G13";
    strncpy(uidev.name, name, sizeof(name));
    uidev.id.bustype = BUS_USB;
    uidev.id.vendor = G13_VENDOR;
    uidev.id.product = G13_PRODUCT;
    uidev.id.version = 1;
    //uidev.absmin[ABS_X] = 0;
    //uidev.absmin[ABS_Y] = 0;
    //uidev.absmax[ABS_X] = 0xff;
    //uidev.absmax[ABS_Y] = 0xff;
  
    
    ret = write(fd, &uidev, sizeof(struct uinput_user_dev));
    if(ret < 0) {
        printf("Unable to write Info\n");
        return 0;
    }
    
    //allow key press / release:
    ret = ioctl(fd, UI_SET_EVBIT, EV_KEY);
    if(ret < 0) {
        printf("Unable to allow key presses.");
        return 0;
    }
    
    //ioctl(fd, UI_SET_MSCBIT, MSC_SCAN);
    
    //for(int i=KEY_RESERVED+1;i<KEY_MEDIA;i++) {
    for(int i=0; i<256; i++) {
        //if(i != 84 AND !(i < 200 AND i > 194) ) {
            ret = ioctl(fd, UI_SET_KEYBIT, i);
            if(ret < 0) {
                printf("Unable to enable Key %d\n", i);
                return 0;
            }else{
                //printf("%d,", i);
            }
        //}
    }
    //printf("\n");
    //ioctl(fd, UI_SET_KEYBIT, BTN_THUMB);
    
    /*ret = ioctl(fd, UI_SET_EVBIT, EV_SYN);
    if(ret < 0) {
        printf("Unable to allow synchro.");
        return 0;
    }*/
    
    
    ret = ioctl(fd, UI_DEV_CREATE);
    if(ret < 0) {
        printf("Unable to create device\n");
        return 0;
    }
    
    sleep(2);

	printf("Hallo!\n");

			memset(&ev, 0, sizeof(struct input_event));
            gettimeofday(&ev.time, 0);
            ev.type = EV_KEY;
            ev.code = KEY_A; 
            ev.value = 1;
			write(fd, &ev, sizeof(struct input_event)) != sizeof(struct input_event);

			memset(&ev, 0, sizeof(struct input_event));
			ev.type = EV_SYN;
			ev.code = SYN_REPORT;
			ev.value = 0;
			write(fd, &ev, sizeof(struct input_event));



			memset(&ev, 0, sizeof(struct input_event));
            gettimeofday(&ev.time, 0);
            ev.type = EV_KEY;
            ev.code = KEY_A;  
            ev.value = 0;
			write(fd, &ev, sizeof(struct input_event)) != sizeof(struct input_event);
		
			memset(&ev, 0, sizeof(struct input_event));
			ev.type = EV_SYN;
			ev.code = SYN_REPORT;
			ev.value = 0;
			write(fd, &ev, sizeof(struct input_event));

			sleep(2);

			printf("Tschüss!\n");

	

}
