/* 
 * File:   globalvariable.h
 * Author: bifi
 *
 * Created on 12. Februar 2013, 21:40
 */

#ifndef GLOBALVARIABLE_H
#define	GLOBALVARIABLE_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "g13constants.h"

bool gKeyData[1+G13_GKEYS];
bool mKeyData[1+G13_MKEYS];
bool pKeyData[1+G13_PKEYS];
bool old_gKeyData[1+G13_GKEYS];
bool joyData[G13_JOYSTICK_AXES][2];
int joyBindings[1+G13_MKEYS][G13_JOYSTICK_AXES][2];
int keyBindings[1+G13_MKEYS][1+G13_GKEYS];
char prog[1+G13_PKEYS][120];

int activeM=1;

int verbose=0;
int verbosebuffer=0;

int fd;
struct uinput_user_dev uidev;
struct input_event ev;

libusb_device_handle *dev_handle; //a device handle
libusb_context *ctx = NULL; //a libusb session


#ifdef	__cplusplus
}
#endif

#endif	/* GLOBALVARIABLE_H */

